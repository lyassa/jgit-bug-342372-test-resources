# Eclipse JGit Bug 342372 Resources #

## Overview ##

This repository provides resources for testing Eclipse JGit [Bug 342372](https://bugs.eclipse.org/bugs/show_bug.cgi?id=342372), "support gitattributes".

## File Descriptions ##

A `.gitattributes` file is provided, which defines some, but not all, of the file types. The following resource files are provided:

* `test.bat`: This file type is defined in .git attributes and should always have `CRLF` line endings.
* `test.foo: This file type is *not* defined in .git attributes but should be auto-detected as text and should have platform-specific line endings.
* `test.jpg`: This file type is defined in .git attributes and should be recognized as binary; its contents should not change on any platform.
* `test.png`: This file type is *not* defined in .git attributes but should be auto-detected as binary; its contents should not change on any platform.
* `test.sh`: This file type is defined in .git attributes and should always have `LF` line endings.
* `test.txt`: This file type is defined in .git attributes and should have platform-specific line endings.

## Preparation ##

1. Clone the [jgit and egit repositories](https://wiki.eclipse.org/EGit/Contributor_Guide#Repositories).
2. [Fetch the following Gerrit changes](https://wiki.eclipse.org/EGit/User_Guide#Fetching_a_change_from_a_Gerrit_Code_Review_Server) which are to be tested:
    * https://git.eclipse.org/r/#/c/60617/
    * https://git.eclipse.org/r/#/c/60635/
4. Install an independent installation of Eclipse.
5. Uninstall the EGit plugin.
6. [Build jgit and egit](https://wiki.eclipse.org/EGit/Contributor_Guide#Maven_Build_Sequence) using Maven.
7. Install your local EGit from `org.eclipse.egit.repository/target/repository`.
8. Fork this current repository (Eclipse JGit Bug 342372 Resources) so that you can work with it locally.

See [Bug 342372 Comment 133](https://bugs.eclipse.org/bugs/show_bug.cgi?id=342372#c133) for more information.

## Test Procedure ##

The following procedure should be followed, 1) first using Git on the command line to verify that it works correctly, and 2) then using the EGit built from the instructions above.

1. Clone your forked version of this repository to a new local location.
2. Verify the line endings of each file listed above under **File Descriptions**.
3. Edit each of the text files listed above under **File Descriptions** by duplicating one of the repeated lines.
4. Commit your changes and push them to the remote repository.
5. Clone the updated remote repository to yet another local location.
6. Verify the line endings of each file listed above under **File Descriptions**.
